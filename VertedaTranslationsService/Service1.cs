﻿using MenuSync;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace VertedaTranslationsService
{
    public partial class Service1 : ServiceBase
    {
        int glb_changed_flag = 0;
        string initialisePrinter = (char)27 + "@" + (char)0;
        string cutPrinter = (char)29 + "V" + (char)0;
        string quadNewLine = Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine;
        FileSystemWatcher m_Watcher = new FileSystemWatcher();

        public Service1()
        {
            InitializeComponent();

            VertedaDiagnostics.OpenLog(ConfigurationManager.AppSettings["LOG_Directory"], "VTS.log");

            VertedaDiagnostics.LogMessage("=======================LOAD=======================", "I", false);
            VertedaDiagnostics.LogMessage("VSI Reporting Started", "I", false);

        }

        public void OnDebug()
        {
            OnStart(null);
        }

        public void LaunchWatcher()
        {
            
            m_Watcher.Path = ConfigurationManager.AppSettings["Receipt_Path"];
            m_Watcher.Filter = ConfigurationManager.AppSettings["Receipt_File"];

            m_Watcher.NotifyFilter = NotifyFilters.LastAccess |
                         NotifyFilters.LastWrite |
                         NotifyFilters.FileName |
                         NotifyFilters.DirectoryName;

            m_Watcher.Changed += new FileSystemEventHandler(OnChanged);

            m_Watcher.EnableRaisingEvents = true;

        }

        private void OnChanged(object sender, FileSystemEventArgs e)
        {
            //prevent multiple fire events on file changed
            glb_changed_flag += 1;

            if (glb_changed_flag == 1)
            {
                //initialise printer per receipt
                Print(initialisePrinter, true);

                //Read xml translations
                List<ComboboxItem> XML_translations = ReadXML();

                //read file contents into list<string> & filter each string for names
                List<string> full_receipt = ReadFile(XML_translations);

                SaveFile(full_receipt);

                foreach (string line in full_receipt)
                {
                    Print(line, false);
                    VertedaDiagnostics.LogMessage("PRINT: " + line, "I", false);
                }
                Print(quadNewLine, true);
                Print(cutPrinter, true);
            }

            int rec_flag;
            bool ps = int.TryParse(ConfigurationManager.AppSettings["Multi_Receipt_flag"], out rec_flag);

            if (ps)
            {
                if (glb_changed_flag < rec_flag)
                {
                    //do nothing
                }
                else
                {
                    glb_changed_flag = 0;
                }
            }
            else
            {
                VertedaDiagnostics.LogMessage("Multi_Receipt_Flag is not a valid integer", "I", false);
            }
        }



        private void SaveFile(List<string> file_output)
        {
            try
            {
                StreamWriter sw = new StreamWriter(ConfigurationManager.AppSettings["Output_receipt_Path"]);
            
                foreach (string line in file_output)
                {
                    sw.WriteLine(line);
                }
                sw.Close();
                VertedaDiagnostics.LogMessage("Translated receipt successfully saved at " + DateTime.Now.ToString(), "I", false);
            }
            catch
            {
                VertedaDiagnostics.LogMessage("Couldn't save translated receipt file to specified path", "I", false);
            }
        }

        public virtual void Print(String line, bool is_instruction)
        {
            try
            {
                StringReader sr = new StringReader(line);

                using (sr)
                {
                    byte[] arabic = Convert(line);

                    using (SerialPort mPort = new SerialPort(
                        "COM" + ConfigurationManager.AppSettings["com_port"],
                        9600,
                        Parity.None,
                        8,
                        StopBits.One))
                    {
                        mPort.DtrEnable = true;

                        //try 3 times to print (in case the COM port is locked)
                        int portOpenAttempts = 0;

                        while (portOpenAttempts <= 3)
                        {
                            try
                            {
                                mPort.Open();
                                break;
                            }
                            catch (Exception ex)
                            {
                                VertedaDiagnostics.LogMessage("Open COM port error", "I", false);
                            }

                            portOpenAttempts++;
                            Thread.Sleep(250);
                        }


                        if (mPort.IsOpen)
                        {
                            if (is_instruction)
                            {
                                mPort.WriteLine(line);
                            }
                            else
                            {
                                mPort.Write(arabic, 0, arabic.Length);
                                mPort.Write(Environment.NewLine);
                            }
                            mPort.Close();
                                //mPort.Dispose();
                            
                            sr.Close();

                            return;
                        }
                        else
                        {
                            VertedaDiagnostics.LogMessage("Could not print receipt", "I", false);

                            sr.Close();

                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                VertedaDiagnostics.LogMessage("Receipt print error: " + ex.Message, "I", false);
            }
        }

        private byte[] Convert(string input)
        {
            byte[] ba = Encoding.Default.GetBytes(input);

            var hexstring = BitConverter.ToString(ba);
            hexstring = hexstring.Replace("-", "");

            return ba;
        }

        private List<string> ReadFile(List<ComboboxItem> xml_translations) 
        {
            List<string> fileContents = new List<string>();
            int divider_flag = 0;

            //pos terminals are slow at releasing printFile
            int readAttempts = 0;

            while (readAttempts <= 3)
            {
                try
                {
                    foreach (var line in File.ReadLines(ConfigurationManager.AppSettings["Receipt_Path"] + ConfigurationManager.AppSettings["Receipt_File"]))
                    {
                        if (line.Contains("===="))
                            divider_flag += 1;

                        VertedaDiagnostics.LogMessage("READ: " + line, "I", false);

                        string t_line = line.Trim();
                        //if we are looking at product section
                        if (divider_flag == 2 && t_line != "")
                        {
                            //if the line starts with a quantity or an asterix                          

                            if (char.IsNumber(t_line, 0) || t_line.StartsWith("*"))
                            {
                                string s = t_line;

                                //if string starts with *, remove the first '*__'
                                if (t_line.StartsWith("*"))
                                {
                                    s = s.Substring(3, s.Length - 3);
                                }

                                int i = s.IndexOf(" ");
                                if (i > 0)
                                {
                                    i += 1;
                                    s = s.Substring(i, s.Length - i);
                                }

                                int x = s.IndexOf("  ");
                                if (x > 0)
                                {
                                    s = s.Substring(0, x);
                                }

                                //s = trimmed product, look up
                                //find match between product Id and xml ID
                                //xml translations, value = product receipt name, text = translation
                                var xml_match = xml_translations.Where(p => p.Value == s).ToList();

                                if (xml_match.Count > 0)
                                {
                                    //succesful translation
                                    fileContents.Add(line);
                                    //string trans_line = line.Replace(s, xml_match[0].Text);
                                    int prod_pos = line.IndexOf(s) + xml_match[0].Text.Length;
                                    string trans_line = xml_match[0].Text.PadLeft(prod_pos);

                                    //trans_line = trans_line.PadLeft(prod_pos);

                                    fileContents.Add(trans_line);
                                }
                                else
                                {
                                    //translation missing
                                    //add 2 lines for normal + missing translation
                                    fileContents.Add(line);
                                    fileContents.Add(ConfigurationManager.AppSettings["Missing_Translation"]);
                                    VertedaDiagnostics.LogMessage("Translation missing for: " + line, "I", false);
                                }
                            }
                            else
                            {
                                //line is blank
                                fileContents.Add(line);
                            }
                        }
                        else
                        {
                            //line isn't in product section, and doesn't meet criteria
                            fileContents.Add(line);

                        }
                    }
                    break;
                }
                catch (Exception ex)
                {
                    VertedaDiagnostics.LogMessage("File not found or empty " + DateTime.Now.ToString(), "E", false);
                }

                readAttempts++;

                // sleep to give IG time to release file
                int sl;
                bool bsl = Int32.TryParse(ConfigurationManager.AppSettings["Sleep_Length"],out sl);

                if (bsl)
                    Thread.Sleep(sl);
                else
                    Thread.Sleep(500);


                VertedaDiagnostics.LogMessage("Read attempt fail: " + readAttempts.ToString() , "I", false);
            }                

            VertedaDiagnostics.LogMessage("Read finished " + DateTime.Now.ToString(), "I", false);
            return fileContents;
        }

        private List<ComboboxItem> LoadProducts()
        {
            List<ComboboxItem> product_list = new List<ComboboxItem>();
            SqlConnection conn = new SqlConnection(GetConnectionString());

            if (conn.State != ConnectionState.Open)
                try
                {
                    conn.Open();
                }
                catch (SqlException e)
                {
                    VertedaDiagnostics.LogMessage("Could not open SQL connection", "I", false);
                }

            if (conn.State == ConnectionState.Open)
            {
                SqlCommand cmd = new SqlCommand(ConfigurationManager.AppSettings["MENU_ITEM_Query"], conn);
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    // if fields queried from database increases, update string array size.
                    string[] rowValues = new string[3];
                    for (int i = 0; i < rdr.FieldCount; i++)
                    {
                        if (rdr.IsDBNull(i))
                        {
                            rowValues[i] = "NULL"; // LOG THAT VALUE IS NULL
                        }
                        else
                            rowValues[i] = rdr.GetValue(i).ToString();
                    }

                    ComboboxItem prod_item = new ComboboxItem();
                    prod_item.Value = rowValues[0];
                    prod_item.Text = rowValues[1];

                    product_list.Add(prod_item);
                }
                rdr.Close();
                conn.Close();
            }
            return product_list;
        }

        public static string GetConnectionString()
        {
            string connStr;
            //// get mysql connection details from config file. 
            connStr = String.Format("Server={0};Uid={1};Pwd={2};Database={3}; Integrated Security=false",
                             ConfigurationManager.AppSettings["MENU_ITEM_IP"],
                             ConfigurationManager.AppSettings["MENU_ITEM_User"],
                             ConfigurationManager.AppSettings["MENU_ITEM_Password"],
                             ConfigurationManager.AppSettings["MENU_ITEM_Database"]);
            return connStr;
        }

        private List<ComboboxItem> ReadXML()
        {
            List<ComboboxItem> li_translations = new List<ComboboxItem>();

            if (!File.Exists(ConfigurationManager.AppSettings["XML_Path"]))
            {
                VertedaDiagnostics.LogMessage("XML translations file not found. Check XML_Path", "I", false);
            }
            else
            {
                //open existing
                XDocument doc = XDocument.Load(ConfigurationManager.AppSettings["XML_Path"]);

                foreach (XElement el in doc.Root.Elements())
                {
                    ComboboxItem PT = new ComboboxItem();
                    PT.Value = el.Element("Name").Value;
                    PT.Text = el.Element("Translation").Value;

                    li_translations.Add(PT);
                }
            }
            VertedaDiagnostics.LogMessage("XML translations read in successfully", "I", false);
            return li_translations;
        }

        protected override void OnStart(string[] args)
        {
            //start filewatcher
            LaunchWatcher();
            VertedaDiagnostics.LogMessage("==================FILE WATCHER ON=================", "I", false);
        }

        protected override void OnStop()
        {
            //stop filewatcher
            m_Watcher.EnableRaisingEvents = false;
            VertedaDiagnostics.LogMessage("=================FILE WATCHER OFF=================", "I", false);
        }
    }

    public class ComboboxItem
    {
        public string Text { get; set; }
        public string Value { get; set; }

        public override string ToString()
        {
            return Text;
        }
    }
}
